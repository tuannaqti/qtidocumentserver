﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WinformServer;

namespace MvcServer.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Word()
        {
            ViewBag.Message = "Download word file.";

            return View();
        }


        [HttpGet]
        public HttpResponseMessage Generate()
        {
            var fileInfo = WordHelp.CreateDoc("", "", "", "", "", "", "", "", "");

            //Process.Start(fileInfo.FullName);

            var stream = new MemoryStream();
            // processing the stream.

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.GetBuffer())
            };
            result.Content.Headers.ContentDisposition =
                new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = fileInfo.FullName// "CertificationCard.pdf"
                };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }
        /// <summary>
        /// Hàm Xuất phiếu thu phí sang Word
        /// </summary>
        /// <param name="MaHoSo"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<FileResult> GetFileFromDisk_Doc(string MaHoSo)
        {
            // await GetTicketData(MaHoSo);
            var ticket = await GetTicketData(MaHoSo);
            var profile = await GetTTCaNhan(ticket.profileID);
            var fileInfo = WordHelp.CreateDoc(GetTTPhieu(ticket.soTTPhieu), ticket.procedureName, ticket.firmName, "MST", profile.fullname == null ? "" : profile.fullname, profile.address1 == null ? "" : profile.address1, ticket.soTien.ToString("N0"), So_chu(ticket.soTien), ticket.hinhThucThanhToan);            
            return new FilePathResult(fileInfo.FullName, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        }

        /// <summary>
        /// Hàm Xuất phiếu thu phí sang PDF
        /// </summary>
        /// <param name="MaHoSo"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<FileResult> XuatPhieuThuPhi(string MaHoSo)
        {

            //var ff = WordHelp.CreatePdf("", "", "", "", "", "", "", "", "");

            // await GetTicketData(MaHoSo);
            var ticket = await GetTicketData(MaHoSo);
            var profile = await GetTTCaNhan(ticket.profileID);
            var procedure = await GetTTThuTuc(ticket.procedure);

            var fileInfo = WordHelp.CreatePdf(GetTTPhieu(ticket.soTTPhieu), ticket.procedureName, ticket.firmName, 
                (procedure ==null ?  "MST" : procedure.MST) , 
                (profile == null ? "" : profile.fullname), 
                (profile == null ? "" : profile.address1), 
                ticket.soTien.ToString("N0"), So_chu(ticket.soTien), ticket.hinhThucThanhToan);
            return new FilePathResult(fileInfo.FullName, "application/pdf");
        }
       
        public static async Task<TicketInfo> GetTicketData(string maBienNhan)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var idDonVi = ""; var maHoSo = maBienNhan;//"1606290000004";
                    var paras = string.Concat("idDonVi=", idDonVi, "&maHoSo=", maHoSo);
                    client.BaseAddress = new Uri("http://beta.dichvucong.quangnam.gov.vn");
                    var response = await client.GetAsync(string.Concat("/expway/smartcloud/glbgate/qti/vtkiostracuu.cpx?", paras));
                    response.EnsureSuccessStatusCode(); // Throw in not success
                    var stringResponse = await response.Content.ReadAsStringAsync();
                    var responseContent = JsonConvert.DeserializeObject<vtkiostracuuModel>(stringResponse);
                    Console.WriteLine($"Got result data: {responseContent.total} items");
                    if (responseContent.total > 0)
                    {
                        return responseContent.items.FirstOrDefault();
                    }
                    return null;
                    //Console.WriteLine($"First post is {JsonConvert.SerializeObject(posts.First())}");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Request exception: {e.Message}");
                    return null;
                }
            }
        }
        public static async Task<NguoiNop> GetTTCaNhan(string profileID)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var profile = profileID;//"1606290000004";
                    var paras = string.Concat("profileID=", profile);
                    client.BaseAddress = new Uri("http://beta.dichvucong.quangnam.gov.vn");
                    var response = await client.GetAsync(string.Concat("/expway/smartcloud/glbgate/userprofile/loadbyid.cpx?", paras));
                    response.EnsureSuccessStatusCode(); // Throw in not success
                    var stringResponse = await response.Content.ReadAsStringAsync();
                    var responseContent = JsonConvert.DeserializeObject<NguoiNop>(stringResponse);                  
                    return responseContent;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Request exception: {e.Message}");
                    return null;
                }
            }
        }
        public static async Task<ThuTuc> GetTTThuTuc(string procedureID)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var procedure = procedureID;//"1606290000004";
                    var paras = string.Concat("procedureID=", procedure);
                    client.BaseAddress = new Uri("http://beta.dichvucong.quangnam.gov.vn");
                    var response = await client.GetAsync(string.Concat("/expway/smartcloud/glbgate/userprofile/loadbyid.cpx?", paras));
                    response.EnsureSuccessStatusCode(); // Throw in not success
                    var stringResponse = await response.Content.ReadAsStringAsync();
                    var responseContent = JsonConvert.DeserializeObject<ThuTuc>(stringResponse);
                    return responseContent;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Request exception: {e.Message}");
                    return null;
                }
            }
        }
        public class vtkiostracuuModel
        {
            public int total { get; set; }
            public TicketInfo[] items { get; set; }
        }
        public class ThuTuc
        {
            public string procedureID { get; set; }
            public string MST { get; set; }
            public string procedureName { get; set; }            
        }
        public class NguoiNop
        {
            public string uuid { get; set; }
            public string authorizedKey { get; set; }
            public string fullname { get; set; }
            public string sex { get; set; }
            public string region { get; set; }
            public string nationality { get; set; }
            public string birthday { get; set; }
            public string identity { get; set; }
            public string idDateOfIssue { get; set; }
            public string idPlaceOfIssuse { get; set; }
            public string otherNumber { get; set; }
            public string otherDateOfIssue { get; set; }
            public string otherPlaceOfIssuse { get; set; }
            public string company { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string mobile { get; set; }
            public string email { get; set; }
        }
        public class TicketInfo
        {
            public bool iSXacnhan { get; set; }
            public bool iSNopphi { get; set; }
            public int soTTPhieu { get; set; }
            public double soTien { get; set; }
            public string hinhThucThanhToan { get; set; }
            public long ngaynopphi { get; set; }
            public string nguoiThu { get; set; }
            public bool overdue { get; set; }
            public string uuid { get; set; }
            public long lastUpdated { get; set; }
            public bool onlineService { get; set; }
            public History[] history { get; set; }
            public object[] payment { get; set; }
            public object[] additional { get; set; }
            public string subject { get; set; }
            public string ticketCode { get; set; }
            public string ticketID { get; set; }
            public string serialNo { get; set; }
            public string receiptNo { get; set; }
            public long created { get; set; }
            public long completed { get; set; }
            public long delivered { get; set; }
            public string profileID { get; set; }
            public string firmName { get; set; }
            public string firmAddress { get; set; }
            public string firmUUID { get; set; }
            public string creator { get; set; }
            public string procedure { get; set; }
            public string procedureName { get; set; }
            public string industry { get; set; }
            public int totalTime { get; set; }
            public long appointmentTime { get; set; }
            public string status { get; set; }
            public bool isPostOffice { get; set; }
            public string type { get; set; }
        }

        public class History
        {
            public string firmUUID { get; set; }
            public string firmName { get; set; }
            public string department { get; set; }
            public string brief { get; set; }
            public long created { get; set; }
            public int totalTime { get; set; }
            public long appointmentTime { get; set; }
            public long completed { get; set; }
            public string sender { get; set; }
            public string executor { get; set; }
        }
        /// <summary>
        /// Hàm hiển thị số thứ tự phiếu thu
        /// </summary>
        /// <param name="sttphieu"></param>
        /// <returns></returns>
        public string GetTTPhieu(int sttphieu)
        {
            try
            {
                switch (sttphieu.ToString().Length)
                {
                    case 1:
                        return "000000" + sttphieu.ToString();
                    case 2:
                        return "00000" + sttphieu.ToString();
                    case 3:
                        return "0000" + sttphieu.ToString();
                    case 4:
                        return "000" + sttphieu.ToString();
                    case 5:
                        return "00" + sttphieu.ToString();
                    case 6:
                        return "0" + sttphieu.ToString();
                    case 7:
                        return sttphieu.ToString();
                    default:
                        return "";

                }
            }
            catch
            {
                return "";
            }

        }
        /// <summary>
        /// Hàm hổ trợ xuất số tiền sang chữ
        /// </summary>
        /// <param name="gNumber"></param>
        /// <returns></returns>
        private static string Chu(string gNumber)
        {
            string result = "";
            switch (gNumber)
            {
                case "0":
                    result = "không";
                    break;
                case "1":
                    result = "một";
                    break;
                case "2":
                    result = "hai";
                    break;
                case "3":
                    result = "ba";
                    break;
                case "4":
                    result = "bốn";
                    break;
                case "5":
                    result = "năm";
                    break;
                case "6":
                    result = "sáu";
                    break;
                case "7":
                    result = "bảy";
                    break;
                case "8":
                    result = "tám";
                    break;
                case "9":
                    result = "chín";
                    break;
            }
            return result;
        }
        /// <summary>
        /// Hàm hổ trợ xuất số tiền sang chữ
        /// </summary>
        /// <param name="so"></param>
        /// <returns></returns>
        private static string Donvi(string so)
        {
            string Kdonvi = "";

            if (so.Equals("1"))
                Kdonvi = "";
            if (so.Equals("2"))
                Kdonvi = "nghìn";
            if (so.Equals("3"))
                Kdonvi = "triệu";
            if (so.Equals("4"))
                Kdonvi = "tỷ";
            if (so.Equals("5"))
                Kdonvi = "nghìn tỷ";
            if (so.Equals("6"))
                Kdonvi = "triệu tỷ";
            if (so.Equals("7"))
                Kdonvi = "tỷ tỷ";

            return Kdonvi;
        }
        /// <summary>
        /// Hàm hổ trợ xuất số tiền sang chữ
        /// </summary>
        /// <param name="tach3"></param>
        /// <returns></returns>
        private static string Tach(string tach3)
        {
            string Ktach = "";
            if (tach3.Equals("000"))
                return "";
            if (tach3.Length == 3)
            {
                string tr = tach3.Trim().Substring(0, 1).ToString().Trim();
                string ch = tach3.Trim().Substring(1, 1).ToString().Trim();
                string dv = tach3.Trim().Substring(2, 1).ToString().Trim();
                if (tr.Equals("0") && ch.Equals("0"))
                    Ktach = " không trăm lẻ " + Chu(dv.ToString().Trim()) + " ";
                if (!tr.Equals("0") && ch.Equals("0") && dv.Equals("0"))
                    Ktach = Chu(tr.ToString().Trim()).Trim() + " trăm ";
                if (!tr.Equals("0") && ch.Equals("0") && !dv.Equals("0"))
                    Ktach = Chu(tr.ToString().Trim()).Trim() + " trăm lẻ " + Chu(dv.Trim()).Trim() + " ";
                if (tr.Equals("0") && Convert.ToInt32(ch) > 1 && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = " không trăm " + Chu(ch.Trim()).Trim() + " mươi " + Chu(dv.Trim()).Trim() + " ";
                if (tr.Equals("0") && Convert.ToInt32(ch) > 1 && dv.Equals("0"))
                    Ktach = " không trăm " + Chu(ch.Trim()).Trim() + " mươi ";
                if (tr.Equals("0") && Convert.ToInt32(ch) > 1 && dv.Equals("5"))
                    Ktach = " không trăm " + Chu(ch.Trim()).Trim() + " mươi lăm ";
                if (tr.Equals("0") && ch.Equals("1") && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = " không trăm mười " + Chu(dv.Trim()).Trim() + " ";
                if (tr.Equals("0") && ch.Equals("1") && dv.Equals("0"))
                    Ktach = " không trăm mười ";
                if (tr.Equals("0") && ch.Equals("1") && dv.Equals("5"))
                    Ktach = " không trăm mười lăm ";
                if (Convert.ToInt32(tr) > 0 && Convert.ToInt32(ch) > 1 && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm " + Chu(ch.Trim()).Trim() + " mươi " + Chu(dv.Trim()).Trim() + " ";
                if (Convert.ToInt32(tr) > 0 && Convert.ToInt32(ch) > 1 && dv.Equals("0"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm " + Chu(ch.Trim()).Trim() + " mươi ";
                if (Convert.ToInt32(tr) > 0 && Convert.ToInt32(ch) > 1 && dv.Equals("5"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm " + Chu(ch.Trim()).Trim() + " mươi lăm ";
                if (Convert.ToInt32(tr) > 0 && ch.Equals("1") && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm mười " + Chu(dv.Trim()).Trim() + " ";

                if (Convert.ToInt32(tr) > 0 && ch.Equals("1") && dv.Equals("0"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm mười ";
                if (Convert.ToInt32(tr) > 0 && ch.Equals("1") && dv.Equals("5"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm mười lăm ";

            }


            return Ktach;

        }
        /// <summary>
        /// Hàm chuyển đổi số tiền sang chữ
        /// </summary>
        /// <param name="gNum"></param>
        /// <returns></returns>
        public static string So_chu(double gNum)
        {
            if (gNum == 0)
                return "Không đồng";

            string lso_chu = "";
            string tach_mod = "";
            string tach_conlai = "";
            double Num = Math.Round(gNum, 0);
            string gN = Convert.ToString(Num);
            int m = Convert.ToInt32(gN.Length / 3);
            int mod = gN.Length - m * 3;
            string dau = "[+]";

            // Dau [+ , - ]
            if (gNum < 0)
                dau = "[-]";
            dau = "";

            // Tach hang lon nhat
            if (mod.Equals(1))
                tach_mod = "00" + Convert.ToString(Num.ToString().Trim().Substring(0, 1)).Trim();
            if (mod.Equals(2))
                tach_mod = "0" + Convert.ToString(Num.ToString().Trim().Substring(0, 2)).Trim();
            if (mod.Equals(0))
                tach_mod = "000";
            // Tach hang con lai sau mod :
            if (Num.ToString().Length > 2)
                tach_conlai = Convert.ToString(Num.ToString().Trim().Substring(mod, Num.ToString().Length - mod)).Trim();

            ///don vi hang mod
            int im = m + 1;
            if (mod > 0)
                lso_chu = Tach(tach_mod).ToString().Trim() + " " + Donvi(im.ToString().Trim());
            /// Tach 3 trong tach_conlai

            int i = m;
            int _m = m;
            int j = 1;
            string tach3 = "";
            string tach3_ = "";

            while (i > 0)
            {
                tach3 = tach_conlai.Trim().Substring(0, 3).Trim();
                tach3_ = tach3;
                lso_chu = lso_chu.Trim() + " " + Tach(tach3.Trim()).Trim();
                m = _m + 1 - j;
                if (!tach3_.Equals("000"))
                    lso_chu = lso_chu.Trim() + " " + Donvi(m.ToString().Trim()).Trim();
                tach_conlai = tach_conlai.Trim().Substring(3, tach_conlai.Trim().Length - 3);

                i = i - 1;
                j = j + 1;
            }
            if (lso_chu.Trim().Substring(0, 1).Equals("k"))
                lso_chu = lso_chu.Trim().Substring(10, lso_chu.Trim().Length - 10).Trim();
            if (lso_chu.Trim().Substring(0, 1).Equals("l"))
                lso_chu = lso_chu.Trim().Substring(2, lso_chu.Trim().Length - 2).Trim();
            if (lso_chu.Trim().Length > 0)
                lso_chu = dau.Trim() + " " + lso_chu.Trim().Substring(0, 1).Trim().ToUpper() + lso_chu.Trim().Substring(1, lso_chu.Trim().Length - 1).Trim() + " đồng y.";

            return lso_chu.ToString().Trim();

        }

    }
}