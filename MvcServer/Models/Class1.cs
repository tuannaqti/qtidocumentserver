﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcServer.Models
{
    public class Class1
    {

        public class Rootobject
        {
            public string uuid { get; set; }
            public string authorizedKey { get; set; }
            public string fullname { get; set; }
            public string sex { get; set; }
            public string region { get; set; }
            public string nationality { get; set; }
            public string birthday { get; set; }
            public string identity { get; set; }
            public string idDateOfIssue { get; set; }
            public string idPlaceOfIssuse { get; set; }
            public string otherNumber { get; set; }
            public string otherDateOfIssue { get; set; }
            public string otherPlaceOfIssuse { get; set; }
            public string company { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string mobile { get; set; }
            public string email { get; set; }
        }

    }
}