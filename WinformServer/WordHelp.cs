﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinformServer
{
    public class WordHelp
    {
        public static System.IO.FileInfo CreateDoc(string sophieuthu, string loaiphi, string donvithu, string masothue, string nguoinoptien, string diachi,string sotien, string tienbangchu, string hinhthuctt)
        {
            var app = new Microsoft.Office.Interop.Word.Application();
            var doc = new Microsoft.Office.Interop.Word.Document();

            doc = app.Documents.Add(Template: @"C:\qti\qtidocumentserver\Template\bienlaithuphi.docx");

            //app.Visible = true;

            foreach (Microsoft.Office.Interop.Word.Field field in doc.Fields)
            {
                switch (field.Result.Text.ToLower())
                {
                    case "«sophieuthu»":
                        field.Select();
                        app.Selection.TypeText(sophieuthu);
                        break;
                    case "«loaiphi»":
                        field.Select();
                        app.Selection.TypeText(loaiphi.ToString());
                        break;
                    case "«donvithu»":
                        field.Select();
                        app.Selection.TypeText(donvithu.ToString());
                        break;
                    case "«masothue»":
                        field.Select();
                        app.Selection.TypeText(masothue.ToString());
                        break;
                    case "«nguoinoptien»":
                        field.Select();
                        app.Selection.TypeText(nguoinoptien.ToString());
                        break;
                    case "«diachi»":
                        field.Select();
                        app.Selection.TypeText(diachi.ToString());
                        break;
                    case "«sotien»":
                        field.Select();
                        app.Selection.TypeText(sotien.ToString());
                        break;
                    case "«tienbangchu»":
                        field.Select();
                        app.Selection.TypeText(tienbangchu.ToString());
                        break;
                    case "«hinhthuctt»":
                        field.Select();
                        app.Selection.TypeText(hinhthuctt.ToString());
                        break;
                    case "«ngay»":
                        field.Select();
                        app.Selection.TypeText("Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                        break;
                    case "«ngay1»":
                        field.Select();
                        app.Selection.TypeText("Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                        break;
                    default:
                        break;
                }
            }

            var fileName = $@"C:\Windows\Temp\template_{Guid.NewGuid().ToString()}.docx";
            doc.SaveAs(FileName: fileName);

            doc.Close();

            var fileInfo = new System.IO.FileInfo(fileName);

            return fileInfo;

        }
        public static System.IO.FileInfo CreatePdf(string sophieuthu, string loaiphi, string donvithu, string masothue, string nguoinoptien, string diachi, string sotien, string tienbangchu, string hinhthuctt)
        {
            var app = new Microsoft.Office.Interop.Word.Application();
            var doc = new Microsoft.Office.Interop.Word.Document();

            doc = app.Documents.Add(Template: @"C:\docserver\Template\bienlaithuphi.docx");

            //app.Visible = true;

            foreach (Microsoft.Office.Interop.Word.Field field in doc.Fields)
            {
                switch (field.Result.Text.ToLower())
                {
                    case "«sophieuthu»":
                        field.Select();
                        app.Selection.TypeText(sophieuthu);
                        break;
                    case "«loaiphi»":
                        field.Select();
                        app.Selection.TypeText(loaiphi);
                        break;
                    case "«donvithu»":
                        field.Select();
                        app.Selection.TypeText(donvithu);
                        break;
                    case "«masothue»":
                        field.Select();
                        app.Selection.TypeText(masothue);
                        break;
                    case "«nguoinoptien»":
                        field.Select();
                        app.Selection.TypeText(nguoinoptien);
                        break;
                    case "«diachi»":
                        field.Select();
                        app.Selection.TypeText(diachi);
                        break;
                    case "«sotien»":
                        field.Select();
                        app.Selection.TypeText(sotien);
                        break;
                    case "«tienbangchu»":
                        field.Select();
                        app.Selection.TypeText(tienbangchu);
                        break;
                    case "«hinhthuctt»":
                        field.Select();
                        app.Selection.TypeText(hinhthuctt);
                        break;
                    case "«ngay»":
                        field.Select();
                        app.Selection.TypeText("Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                        break;
                    case "«ngay1»":
                        field.Select();
                        app.Selection.TypeText("Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                        break;
                    default:
                        break;
                }
            }

            var fileName = $@"C:\docserver\Template\exported\template_{Guid.NewGuid().ToString()}.pdf";

            doc.ExportAsFixedFormat(fileName, WdExportFormat.wdExportFormatPDF);
            //doc.Close();
            ////doc.SaveAs(FileName: fileName);

            ////doc.SaveAs(fileName, WdSaveFormat.wdFormatPDF);


            //object oMissing = System.Reflection.Missing.Value;
            //Object filename2 = (Object)fileName;

            //doc.SaveAs(ref filename2, WdSaveFormat.wdFormatPDF,
            //           ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
            //           ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
            //           ref oMissing, ref oMissing, ref oMissing, ref oMissing);

            //// close word doc and word app.
            //object saveChanges = WdSaveOptions.wdDoNotSaveChanges;

            //((_Document)doc).Close(ref saveChanges, ref oMissing, ref oMissing);

            //((_Application)app).Quit(ref oMissing, ref oMissing, ref oMissing);

            //app = null;
            //doc = null;
            ////doc.Close();

            var fileInfo = new System.IO.FileInfo(fileName);

            return fileInfo;

        }

    }
}
